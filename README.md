# Own Sentry 

Official bootstrap for running your own [Sentry](https://getsentry.com/) with [Docker](https://www.docker.com/). This repository was forked from [getsentry/onpremise](https://github.com/getsentry/onpremise/) and updated with Sentry v8.7 configuration. 

## Setup

This guide will step you through setting up your own sentry in [Docker](https://www.docker.com/).

### Installation of Docker v1.12

[Documentation](https://docs.docker.com/engine/installation/linux/ubuntulinux/)

If you want to install the docker on the fresh Ubuntu Trusty 14.04[LTS], simply step the followings. 

* Update package information and that CA certificates are installed.

	``` $ sudo apt-get update ```

	``` $ sudo apt-get install apt-transport-https ca-certificates ```

* Add the new GPG key. 

	``` $ sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D ```

* Open the /etc/apt/sources.list.d/docker.list file in your favorite editor. If the file doesn’t exist, create it.

* Add an entry for your Ubuntu operating system.

	``` deb https://apt.dockerproject.org/repo ubuntu-trusty main ```

* Save and close the /etc/apt/sources.list.d/docker.list file.

* Update the APT package index.

	``` $ sudo apt-get update ```

* Install the apparmor. 

	``` $ sudo apt-get install apparmor ```

* Install Docker. 

	``` $ sudo apt-get install docker-engine ```

* Start the docker daemon. 

	``` $ sudo service docker start ```

* Verify docker is installed correctly.

	``` $ sudo docker run hello-world ```

* Check the installed docker version. 

	``` $ sudo docker -v ```

* If the docker verion is lower than 1.12, upgrade it. 

	``` $ sudo apt-get upgrade docker-engine ```

### Installation of Sentry

[Documentation](https://hub.docker.com/_/sentry/)

If you want to install the own sentry v8.7 with disabling the user registration, simply step the followings. 

* Clone this git repository and build the sentry container

	``` $ git clone https://github.com/tony522/own-sentry ```

	``` $ cd own-sentry ```

	``` $ make build ```

* Start a Redis container. 

	``` $ docker run -d --name sentry-redis redis ```

* Start a Postgres container. 

	``` $ docker run -d --name sentry-postgres -e POSTGRES_PASSWORD=sentry -e POSTGRES_USER=sentry postgres ```

* Generate a new secret key to be shared by all sentry containers. This value will then be used as the SENTRY_SECRET_KEY environment variable.

	``` $ docker run --rm sentry config generate-secret-key ```

* If this is a new database, you'll need to run upgrade.

	``` $ docker run -it --rm -e SENTRY_SECRET_KEY='<secret-key>' --link sentry-postgres:postgres --link sentry-redis:redis own-sentry upgrade ```

* Now start up Sentry server.

	``` $ docker run -d --name my-sentry -p 80:9000 -e SENTRY_SECRET_KEY='<secret-key>' --link sentry-redis:redis --link sentry-postgres:postgres own-sentry ```

* The default config needs a celery beat and celery workers, start as many workers as you need (each with a unique name).

	``` $ docker run -d --name sentry-cron -e SENTRY_SECRET_KEY='<secret-key>' --link sentry-postgres:postgres --link sentry-redis:redis own-sentry run cron ```

	``` $ docker run -d --name sentry-worker-1 -e SENTRY_SECRET_KEY='<secret-key>' --link sentry-postgres:postgres --link sentry-redis:redis own-sentry run worker ```

* Verify the status of running docker containers. 

	``` $ docker ps ```

* If all is running correctly, check up the web service by visiting http://ip. 

* If you want to create a superuser manually, use the following. 

	``` $ docker run -it --rm -e SENTRY_SECRET_KEY='<secret-key>' --link sentry-redis:redis --link sentry-postgres:postgres own-sentry createuser ```

### Service operations of docker containers

* If you encounter a frustrating problem in running a specific docker container, simply stop and remove it. Then, you must re-run the container again. (e.g, $ docker run -d -name ...)

	``` $ docker stop <container-name> ```

	``` $ docker rm <container-name> ```

* If you remove all docker containers, you must first stop all running containers and then remove them. 

	``` $ docker stop `docker ps -aq` ```

	``` $ docker stop `docker ps -aq` ```

* If you want to stop all web service by docker, 

	``` $ sudo service docker stop ```

* If you want to resume all web service stopped by docker, 

	``` $ sudo service docker start ```

* If you want to resume the above sentry & relative containers halt by stoping the docker service, you must first re-run the redis & postgres containers and then restart all relative sentry containers. 

	``` $ docker start sentry-redis ```

	``` $ docker start sentry-postgres ```

	``` $ docker start `docker ps -aq` ``` 

### Uninstallation the docker on the Ubuntu 14.04 (LTS)

* To uninstall the Docker package. 

	``` $ sudo apt-get purge docker-engine ```

* To uninstall the Docker package and dependencies that are no longer needed. 

	``` $ sudo apt-get autoremove --purge docker-engine ``` 

* If you wish to delete all images, containers, volumes, or user created configuration files on your host, 

	``` $ sudo rm -rf /var/lib/docker ```




